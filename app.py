from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://yubnlesbbghpyq:2170a40d6e5fad87a6d6ebcbfefa45601f4284c4caa7ff5cbb41283db0f9a4ce@ec2-34-199-68-114.compute-1.amazonaws.com:5432/d4jiurmrtsrtge'
db = SQLAlchemy(app)

@app.route('/', methods=['POST', 'GET'])
def index():
    return render_template('index.html')

if __name__ == "__main__":
    app.run(debug=True)